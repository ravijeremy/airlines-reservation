<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fax', function (Blueprint $table) {
            $table->char('fcrtycode', 2)->unsigned();
            $table->char('fareacode', 3)->unsigned();
            $table->char('fnumber', 7)->unsigned();
            $table->bigInteger('custid')->unsigned();

            $table->primary(['custid', 'fcrtycode', 'fareacode', 'fnumber']);
            $table->foreign('custid')->references('custid')->on('customer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fax');
    }
}
