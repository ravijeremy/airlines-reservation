<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitycurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citycurrency', function (Blueprint $table) {
            $table->bigInteger('cityid')->unsigned();
            $table->char('fcurr', 3)->unsigned();
            $table->char('tcurr', 3)->unsigned();

            $table->primary(['cityid', 'fcurr', 'tcurr']);
            $table->foreign('cityid')->references('cityid')->on('city');
            $table->foreign(['fcurr', 'tcurr'])->references(['fcurr', 'tcurr'])->on('currency');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citycurrency');
    }
}
