<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flight', function (Blueprint $table) {
            $table->string('fno', 10)->primary();
            $table->char('smallow', 1);
            $table->char('bcavl', 1)->nullable();
            $table->string('airlinecd', 10)->unsigned();
            
            $table->foreign('airlinecd')->references('airlinecd')->on('airline');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flight');
    }
}
