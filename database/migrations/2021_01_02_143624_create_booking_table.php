<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->bigIncrements('bkgno')->primary();
            $table->date('bkgdate');
            $table->decimal('bal', 8, 2)->nullable();
            $table->decimal('totprice', 8, 2)->nullable();
            $table->decimal('paidamt', 8, 2);
            $table->decimal('fprice', 8, 2);
            $table->string('fno', 10)->unsigned();
            $table->string('depttime', 30)->unsigned();
            $table->string('arrtime', 30)->unsigned();
            $table->char('dest', 3)->unsigned();
            $table->char('orig', 3)->unsigned();
            $table->bigInteger('bkgcity')->unsigned();
            $table->bigInteger('custid')->unsigned();
            $table->bigInteger('paidby')->unsigned();
            $table->bigInteger('classid')->unsigned();
            $table->bigInteger('statusid')->unsigned();

            $table->foreign(['fno', 'depttime', 'arrtime', 'dest', 'orig'])->references(['fno', 'depttime', 'arrtime', 'dest', 'orig'])->on('flightavailability');
            $table->foreign('bkgcity')->references('cityid')->on('city');
            $table->foreign('custid')->references('custid')->on('customer');
            $table->foreign('paidby')->references('custid')->on('customer');
            $table->foreign('classid')->references('classid')->on('classes');
            $table->foreign('statusid')->references('statusid')->on('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking');
    }
}
