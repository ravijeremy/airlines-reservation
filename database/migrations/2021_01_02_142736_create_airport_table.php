<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airport', function (Blueprint $table) {
            $table->bigInteger('cityid')->unsigned();
            $table->char('airportcd', 3)->primary();
            $table->string('airportnm', 50)->nullable();
            $table->decimal('airporttax', 6, 2);
            
            $table->foreign('cityid')->references('cityid')->on('city');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airport');
    }
}
