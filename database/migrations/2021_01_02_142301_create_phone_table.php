<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phone', function (Blueprint $table) {
            $table->char('pcrtycode', 2)->unsigned();
            $table->char('pareacode', 3)->unsigned();
            $table->char('pnumber', 7)->unsigned();
            $table->bigInteger('custid')->unsigned();

            $table->primary(['custid', 'pcrtycode', 'pareacode', 'pnumber']);
            $table->foreign('custid')->references('custid')->on('customer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phone');
    }
}
