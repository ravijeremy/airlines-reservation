<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightavailabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flightavailability', function (Blueprint $table) {
            $table->string('flen', 30);
            $table->string('depttime', 30)->unsigned();
            $table->string('arrtime', 30)->unsigned();
            $table->string('fno', 10)->unsigned();
            $table->bigInteger('bbusseat');
            $table->bigInteger('becoseat');
            $table->bigInteger('tbusseat');
            $table->bigInteger('tecoseat');
            $table->char('dest', 3)->unsigned();
            $table->char('orig', 3)->unsigned();
            
            $table->primary(['fno', 'dest', 'orig', 'depttime', 'arrtime']);
            $table->foreign('fno')->references('fno')->on('flight');
            $table->foreign('dest')->references('airportcd')->on('airport');
            $table->foreign('orig')->references('airportcd')->on('airport');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flightavailability');
    }
}
