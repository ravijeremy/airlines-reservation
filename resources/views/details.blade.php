@extends('layouts.app')

@section('content')
    <div id="content">
        <div class="row no-gutters"> 
            <div class="container px-4 py-4">
                <p class="font-weight-bold">Booking Details</p>
                <table>
                      <tr>
                        <td style="min-width: 122px">Booking No</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                        <td style="min-width: 122px">Booking Date</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      <tr>
                        <td style="min-width: 122px">Balance</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                      <tr>
                        <td style="min-width: 122px">Total Price</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                      <tr>
                        <td style="min-width: 122px">Paid Amount</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                      <tr>
                        <td style="min-width: 122px">Flight Price</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                      <tr>
                        <td style="min-width: 122px">Flight No</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                      <tr>
                        <td style="min-width: 122px">Departure Time</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                      <tr>
                        <td style="min-width: 122px">Arrival Time</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                      <tr>
                        <td style="min-width: 122px">Destination</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                      <tr>
                        <td style="min-width: 122px">Origin</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                      <tr>
                        <td style="min-width: 122px">Booking City</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                      <tr>
                        <td style="min-width: 122px">Customer ID</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                      <tr>
                        <td style="min-width: 122px">Paid By</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                      <tr>
                        <td style="min-width: 122px">Class</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                      <tr>
                        <td style="min-width: 122px">Status</td>
                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                        <td>$1000</td>
                      </tr>
                </table>
                <a class="btn btn-primary" href="#" role="button">Link</a>
                <button type="submit" class="btn btn-primary">Close</button>
            </div>
        </div>
    </div>
@endsection