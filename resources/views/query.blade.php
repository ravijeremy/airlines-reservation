@extends('layouts.app')

@section('content')
    <div id="content">
        {{-- Navbar --}}
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span>Menu</span>
                </button>
            </div>
        </nav>
        {{-- End of Navbar --}}

        {{-- Interactive Query --}}
        <div class="row no-gutters">
            <div class="container px-4 py-4">
                <form action="{{ route('query_submit') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Interactive Query</label>
                        <textarea type="text" class="form-control" id="queryCustom" name="queryCustom" placeholder="Insert Query"></textarea>
                        <small id="queryHelp" class="form-text text-muted">*You can insert your own query and run them here.</small>
                        <small id="queryHelp" class="form-text text-muted">*Please use a corect SQL Command to get the expected results.</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Run</button>
                </form>
                @isset($queryCustom)
                    <table class="table table-striped table-bordered mt-4">
                        <thead>
                            {{-- <tr>
                                @foreach ($queryCustom as $item)
                                    <td>{{ $item[0] }}</td>
                                @endforeach
                            </tr> --}}
                            {{-- <>
                                @foreach ($queryCustom as $item)
                                    @foreach ($item as $key)
                                        <th>{{ $key }}</th>
                                    @endforeach
                                    @break
                                @endforeach
                            </> --}}
                        </thead>
                        <tbody>
                            <tr>
                                @foreach ($queryCustom as $item)
                                    <td>{{ $item->airportcd }}</td>
                                @endforeach
                            </tr>
                        </tbody>
                    </table>
                @endisset
            </div>
        </div>
        {{-- End of Interactive Query --}}

        {{-- Query Example --}}
        <div class="row no-gutters pt-3">
            <div class="container px-4 py-4">
                <label>Query Examples</label>
                <div class="accordion mr-2" id="accordionExample">
                    {{-- QUERY #1 --}}
                    <div class="card mb-2 rounded">
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Query 1: Give all the customers who lives in Canada.
                                </button>
                            </h2>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <p class="mb-2" style="color: black">
                                    Goal: Give all the customers who lives in Canada and sort by customer_id.
                                    <br>Query:
                                    <br>SELECT * FROM CUSTOMER WHERE COUNTRY='CANADA' ORDER BY CUSTID
                                </p>
                                <table class="table table-striped table-bordered" id="tableQueryOne">
                                    <thead>
                                        <tr>
                                            <th>Customer ID</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Street</th>
                                            <th>City</th>
                                            <th>Province</th>
                                            <th>Country</th>
                                            <th>Postal Code</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($query1) > 0)
                                            @foreach($query1 as $customer)
                                                @if(!empty($customer))
                                                    <tr>
                                                        <td>{{ $customer->custid }}</td>
                                                        <td>{{ $customer->fname }}</td>
                                                        <td>{{ $customer->lname }}</td>
                                                        <td>{{ $customer->street }}</td>
                                                        <td>{{ $customer->city }}</td>
                                                        <td>{{ $customer->province }}</td>
                                                        <td>{{ $customer->country }}</td>
                                                        <td>{{ $customer->postcode }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- END OF QUERY #1 --}}

                    {{-- QUERY #2 --}}
                    <div class="card mb-2 rounded">
                        <div class="card-header" id="headingTwo">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    Query 2: List all different customers who made bookings.
                                </button>
                            </h2>
                        </div>

                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                <p class="mb-2" style="color: black">
                                    Goal: List all different customers who made bookings.
                                    Query:
                                    <br>SELECT * FROM BOOKING INNER JOIN CUSTOMER ON BOOKING.CUSTID=CUSTOMER.CUSTID
                                </p>
                                <table class="table table-striped table-bordered" id="tableQueryTwo">
                                    <thead>
                                        <tr>
                                            <th>Booking Number</th>
                                            <th>Customer ID</th>
                                            <th>First Name</th>
                                            <th>Booking Date</th>
                                            <th>Flight No</th>
                                            <th>Destination</th>
                                            <th>Origin</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($query2) > 0)
                                            @foreach($query2 as $booking)
                                                @if(!empty($booking))
                                                    <tr>
                                                        <td>{{ $booking->bkgno }}</td>
                                                        <td>{{ $booking->custid }}</td>
                                                        <td>{{ $booking->fname }}</td>
                                                        <td>{{ $booking->bkgdate }}</td>
                                                        <td>{{ $booking->fno }}</td>
                                                        <td>{{ $booking->dest }}</td>
                                                        <td>{{ $booking->orig }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- END OF QUERY #2 --}}

                    {{-- QUERY #3 --}}
                    <div class="card mb-2 rounded">
                        <div class="card-header" id="headingThree">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                    Query 3: Display all currency exchange rate is greater than 1.
                                </button>
                            </h2>
                        </div>

                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                <p class="mb-2" style="color: black">
                                    Goal:Display all currency exchange rate is greater than 1. Please sort them by from_currency and to_currency.
                                    <br>Query:
                                    <br>SELECT * FROM CURRENCY WHERE EXCHRATE>1 ORDER BY FCURR AND TCURR
                                </p>
                                <table class="table table-striped table-bordered" id="tableQueryThree">
                                    <thead>
                                        <tr>
                                            <th>From Currency</th>
                                            <th>To Currency</th>
                                            <th>Exchange Rate</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($query3) > 0)
                                            @foreach($query3 as $currency)
                                                @if(!empty($currency))
                                                    <tr>
                                                        <td>{{ $currency->fcurr }}</td>
                                                        <td>{{ $currency->tcurr }}</td>
                                                        <td>{{ $currency->exchrate }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- END OF QUERY #3 --}}

                    {{-- QUERY #4 --}}
                    <div class="card mb-2 rounded">
                        <div class="card-header" id="headingFour">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                    Query 4: List all the flight availabilities between Toronto and New York.
                                </button>
                            </h2>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                            <div class="card-body">
                                <p class="mb-2" style="color: black">
                                    Query
                                    <br>SELECT * FROM FLIGHTAVAILABILITY
                                    <br>INNER JOIN AIRPORT ON FLIGHTAVAILABILITY.ORIG=AIRPORT.AIRPORTCD
                                    <br>WHERE ORIGIN='YYZ' OR ORIGIN='JFK'
                                </p>
                                <table class="table table-striped table-bordered" id="tableQueryFour">
                                    <thead>
                                        <tr>
                                            <th>Airport Code</th>
                                            <th>Flight No.</th>
                                            <th>Origin</th>
                                            <th>Destination</th>
                                            <th>Departure Time</th>
                                            <th>Arrival Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($query4) > 0)
                                            @foreach($query4 as $flight)
                                                @if(!empty($flight))
                                                    <tr>
                                                        <td>{{ $flight->airportcd }}</td>
                                                        <td>{{ $flight->fno }}</td>
                                                        <td>{{ $flight->orig }}</td>
                                                        <td>{{ $flight->dest }}</td>
                                                        <td>{{ $flight->depttime }}</td>
                                                        <td>{{ $flight->arrtime }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- END OF QUERY #4 --}}

                    {{-- QUERY #5 --}}
                    <div class="card mb-2 rounded">
                        <div class="card-header" id="headingFive">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                    Query 5: List all customers who did not place any booking.
                                </button>
                            </h2>
                        </div>

                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                            <div class="card-body">
                                <p class="mb-2" style="color: black">
                                    Goal: List all customers who did not place any booking. Please display customer_id only, and sort records by customer_id.
                                    <br>
                                    Query:
                                    <br>SELECT * FROM CUSTOMER WHERE CUSTID NOT IN (SELECT CUSTID FROM BOOKING)
                                </p>
                                <table class="table table-striped table-bordered" id="tableQueryFive">
                                    <thead>
                                        <tr>
                                            <th>Customer ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($query5) > 0)
                                            @foreach($query5 as $customer)
                                                @if(!empty($customer))
                                                    <tr>
                                                        <td>{{ $customer->custid }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- END OF QUERY #5 --}}

                    {{-- QUERY #6 --}}
                    <div class="card mb-2 rounded">
                        <div class="card-header" id="headingSix">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                                    Query 6: Display all customer's first name, last name, phone number and email.
                                </button>
                            </h2>
                        </div>

                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                            <div class="card-body">
                                <p class="mb-2" style="color: black">
                                    Goal: Display all customer's first_name, last_name, phone_no (format like 416-111-2222) and email. Please sort them by customer_id.
                                    <br>Query:
                                    <br>SELECT CUSTOMER.FNAME, CUSTOMER.LNAME, PHONE.PNUMBER, EMAIL.EMAIL FROM CUSTOMER
                                    <br>INNER JOIN PHONE ON CUSTOMER.CUSTID=PHONE.CUSTID
                                    <br>INNER JOIN EMAIL ON CUSTOMER.CUSTID=EMAIL.CUSTID
                                    <br>ORDER BY CUSTOMER.CUSTID</p>
                                <table class="table table-striped table-bordered" id="tableQuerySix">
                                    <thead>
                                        <tr>
                                            <th>Customer ID</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Phone Number</th>
                                            <th>Email Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($query6) > 0)
                                            @foreach($query6 as $customer)
                                                @if(!empty($customer))
                                                    <tr>
                                                        <td>{{ $customer->custid }}</td>
                                                        <td>{{ $customer->fname }}</td>
                                                        <td>{{ $customer->lname }}</td>
                                                        <td>{{ $customer->pnumber }}</td>
                                                        <td>{{ $customer->email }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- END OF QUERY #6 --}}

                    {{-- QUERY #7 --}}
                    <div class="card mb-2 rounded">
                        <div class="card-header" id="headingSeven">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                                    Query 7: List all canceled bookings.
                                </button>
                            </h2>
                        </div>

                        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                            <div class="card-body">
                                <p style="color: black">
                                    Goal: List all canceled bookings. please display booking_no, customer_id, flight_no, origin, destination, class, status, and booking_city. Please also sort by booking_no, customer_id and flight_no.
                                    <br>Query:
                                    <br>SELECT BOOKING.BKGNO, BOOKING.CUSTID, BOOKING.FNO, BOOKING.ORIG, BOOKING.DEST, STATUS.STATUS, CLASSES.CLASSTYPE, CITY.CITYNM FROM BOOKING
                                    <br>JOIN STATUS ON BOOKING.STATUSID=STATUS.STATUSID
                                    <br>JOIN CLASSES ON BOOKING.CLASSID=CLASSES.CLASSID
                                    <br>JOIN CITY ON BOOKING.BOOKINGCITY=CITY.CITYID
                                </p>
                                <table class="table table-striped table-bordered" id="tableQuerySeven">
                                    <thead>
                                        <tr>
                                            <th>Booking Number</th>
                                            <th>Customer ID</th>
                                            <th>Flight Number</th>
                                            <th>Origin</th>
                                            <th>Destination</th>
                                            <th>Class</th>
                                            <th>Status</th>
                                            <th>Booking City</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($query7) > 0)
                                            @foreach($query7 as $booking)
                                                @if(!empty($booking))
                                                    <tr>
                                                        <td>{{ $booking->bkgno }}</td>
                                                        <td>{{ $booking->custid }}</td>
                                                        <td>{{ $booking->fno }}</td>
                                                        <td>{{ $booking->orig }}</td>
                                                        <td>{{ $booking->dest }}</td>
                                                        <td>{{ $booking->classtype }}</td>
                                                        <td>{{ $booking->status }}</td>
                                                        <td>{{ $booking->bkgcity }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- END OF QUERY #7 --}}

                    {{-- QUERY #8 --}}
                    <div class="card mb-2 rounded">
                        <div class="card-header" id="headingEight">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
                                    Query 8: List total price, total payment and total balance for each city.
                                </button>
                            </h2>
                        </div>

                        <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
                            <div class="card-body">
                                <p style="color: black">
                                    List total_price, total_payment and total_balance for each city. Please exclude canceled bookings and sort records by city_name.
                                    <br>Query:
                                    <br>SELECT BOOKING.BKGCITY, BOOKING.TOTPRICE, BOOKING.PAIDAMT, BOOKING.BAL FROM BOOKING
                                    <br>WHERE NOT STATUSID=3 ORDER BY BKGCITY

                                </p>
                                <table class="table table-striped table-bordered" id="tableQueryEight">
                                    <thead>
                                        <tr>
                                            <th>City Name</th>
                                            <th>Total Price</th>
                                            <th>Total Payment</th>
                                            <th>Total Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($query8) > 0)
                                            @foreach($query8 as $booking)
                                                @if(!empty($booking))
                                                    <tr>
                                                        <td>{{ $booking->bkgcity }}</td>
                                                        <td>{{ $booking->totprice }}</td>
                                                        <td>{{ $booking->paidamt }}</td>
                                                        <td>{{ $booking->bal }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- END OF QUERY #8 --}}

                    {{-- QUERY #9 --}}
                    <div class="card mb-2 rounded">
                        <div class="card-header" id="headingNine">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="true" aria-controls="collapseNine">
                                    Query 9: Calculate new total price for each booking taxes increase.
                                </button>
                            </h2>
                        </div>

                        <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample">
                            <div class="card-body">
                                <p style="color: black">
                                    Calculate new total_price for each booking if origin airport tax increase by 0.01 and destination airport tax decrease by 0.005. Please display booking_no, origin, destination, flight_price, previous_total_price and new_total_price.
                                    <br>Query:
                                    <br>SELECT BOOKING.BKGNO, BOOKING.ORIG, BOOKING.DEST, BOOKING.FPRICE,
                                    <br>BOOKING.TOTPRICE AS PREVIOUSPRICE,
                                    <br>(BOOKING.TOTPRICE + 
                                    <br>BOOKING.FPRICE*(SELECT AIRPORTTAX+0.01 FROM AIRPORT WHERE AIRPORT.AIRPORTCD=BOOKING.ORIG) +
                                    <br>BOOKING.FPRICE*(SELECT AIRPORTTAX+0.005 FROM AIRPORT WHERE AIRPORT.AIRPORTCD=BOOKING.DEST)) AS NEWPRICE
                                    <br>FROM BOOKING
                                </p>
                                <table class="table table-striped table-bordered" id="tableQueryNine">
                                    <thead>
                                        <tr>
                                            <th>Booking Number</th>
                                            <th>Origin</th>
                                            <th>Destination</th>
                                            <th>Flight Price</th>
                                            <th>Previous Total Price</th>
                                            <th>New Total Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($query9) > 0)
                                            @foreach($query9 as $booking)
                                                @if(!empty($booking))
                                                    <tr>
                                                        <td>{{ $booking->bkgno }}</td>
                                                        <td>{{ $booking->orig }}</td>
                                                        <td>{{ $booking->dest }}</td>
                                                        <td>{{ $booking->fprice }}</td>
                                                        <td>{{ $booking->prevprice }}</td>
                                                        <td>{{ $booking->newprice }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- END OF QUERY #9 --}}

                    {{-- QUERY #10 --}}
                    <div class="card mb-2 rounded">
                        <div class="card-header" id="headingTen">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTen" aria-expanded="true" aria-controls="collapseTen">
                                    Query 10: List number of bookings, emails, phones and faxs for each customer.
                                </button>
                            </h2>
                        </div>

                        <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionExample">
                            <div class="card-body">
                                <p style="color: black">
                                    List number_of_bookings, number_of_emails, number_of_phones and number_of_faxs for each customer.
                                    <br>SELECT CUSTOMER.CUSTID, CUSTOMER.FNAME, CUSTOMER.LNAME, COUNT(DISTINCT FAX.FNUMBER) AS FAX, COUNT(DISTINCT BOOKING.BKGNO), COUNT(DISTINCT PHONE.PNUMBER), COUNT(DISTINCT EMAIL.EMAIL) FROM CUSTOMER
                                    <br>FULL OUTER JOIN BOOKING ON BOOKING.CUSTID=CUSTOMER.CUSTID
                                    <br>FULL OUTER JOIN EMAIL ON EMAIL.CUSTID=CUSTOMER.CUSTID
                                    <br>FULL OUTER JOIN FAX ON FAX.CUSTID=CUSTOMER.CUSTID
                                    <br>FULL OUTER JOIN PHONE ON PHONE.CUSTID=CUSTOMER.CUSTID
                                    <br>GROUP BY CUSTOMER.CUSTID, CUSTOMER.FNAME, CUSTOMER.LNAME
                                    <br>ORDER BY CUSTOMER.CUSTID
                                </p>
                                <table class="table table-striped table-bordered" id="tableQueryTen">
                                    <thead>
                                        <tr>
                                            <th>Customer ID</th>
                                            <th>Number of Bookings</th>
                                            <th>Number of Emails</th>
                                            <th>Number of Phones</th>
                                            <th>Number of Faxs</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($query10) > 0)
                                            @foreach($query10 as $customer)
                                                @if(!empty($customer))
                                                    <tr>
                                                        <td>{{ $customer->custid }}</td>
                                                        <td>{{ $customer->totbookings }}</td>
                                                        <td>{{ $customer->totemails }}</td>
                                                        <td>{{ $customer->totphones }}</td>
                                                        <td>{{ $customer->totfaxs }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- END OF QUERY #10 --}}
                </div>
            </div>
        </div>
        {{-- End of Query Example --}}
</div>
@endsection