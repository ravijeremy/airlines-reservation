@extends('layouts.app')

@section('content')
    <div id="content">
        {{-- Navbar --}}
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span>Menu</span>
                </button>
            </div>
        </nav>
        {{-- End of Navbar --}}

        {{-- Customer Input Data --}}
        <div class="row no-gutters">
            <div class="container px-4 py-4">
                <p class="font-weight-bold">Input Customer's Data</p>
                <form action="/input-data" method="POST">
                    @csrf

                    @if($errors->any())
                        <div class="alert alert-danger">
                            <strong>Oops!</strong> Please correct error and try again.
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{-- Name & Email--}}
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="fname">First Name</label>
                                <input type="input" value="{{ old('fname') }}" class="form-control @error('fname') has-error @enderror" id="fname" name="fname" placeholder="First Name">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="lname">Last Name</label>
                                <input type="input" value="{{ old('lname') }}" class="form-control @error('lname') has-error @enderror" id="lname" name="lname" placeholder="Last Name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="emailCustomer">Email</label>
                        <input type="email" value="{{ old('emailCustomer') }}" class="form-control @error('emailCustomer') has-error @enderror" id="emailCustomer" name="emailCustomer" placeholder="Email">
                    </div>

                    {{-- Alamat --}}
                    <p class="font-weight-bold">Mailing Information</p>
                    <div class="form-group">
                        <label for="street">Street</label>
                        <input type="input" value="{{ old('street') }}" class="form-control @error('street') has-error @enderror" id="street" name="street" placeholder="Street">
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="input" value="{{ old('city') }}" class="form-control @error('city') has-error @enderror" id="city" name="city" placeholder="City">
                    </div>
                    <div class="form-group">
                        <label for="province">Province</label>
                        <input type="input" value="{{ old('province') }}" class="form-control @error('province') has-error @enderror" id="province" name="province" placeholder="Province">
                    </div>
                    <div class="form-group">
                        <label for="country">Country</label>
                        <input type="input" value="{{ old('country') }}" class="form-control @error('country') has-error @enderror" id="country" name="country" placeholder="Country">
                    </div>
                    <div class="form-group">
                        <label for="postcode">Postal Code</label>
                        <input type="number" value="{{ old('postcode') }}" class="form-control @error('postcode') has-error @enderror" id="postcode" name="postcode" placeholder="Postal Code">
                    </div>

                    {{-- Phone --}}
                    <p class="font-weight-bold">Phone Information</p>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="pctrycode">Phone Country Code</label>
                                <input type="number" value="{{ old('pctrycode') }}" class="form-control @error('pcrtycode') has-error @enderror" id="pctrycode" name="pctrycode" placeholder="Phone Country Code">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="pareacode">Phone Number Area</label>
                                <input type="number" value="{{ old('pareacode') }}" class="form-control @error('pareacode') has-error @enderror" id="pareacode" name="pareacode" placeholder="Phone Number Area">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="pnumber">Phone Number</label>
                                <input type="number" value="{{ old('pnumber') }}" class="form-control @error('pnumber') has-error @enderror" id="pnumber" name="pnumber" placeholder="Phone Number">
                            </div>
                        </div>
                    </div>

                    {{-- Phone --}}
                    <p class="font-weight-bold">Fax Information</p>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="fctrycode">Fax Country Code</label>
                                <input type="number" value="{{ old('fctrycode') }}" class="form-control @error('fcrtycode') has-error @enderror" id="fctrycode" name="fctrycode" placeholder="Fax Country Code">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="fareacode">Fax Number Area</label>
                                <input type="number" value="{{ old('fareacode') }}" class="form-control @error('fareacode') has-error @enderror" id="fareacode" name="fareacode" placeholder="Fax Number Area">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="fnumber">Fax Number</label>
                                <input type="number" value="{{ old('fnumber') }}" class="form-control @error('fnumber') has-error @enderror" id="fnumber" name="fnumber" placeholder="Fax Number">
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        {{-- End of Customer Input Data --}}
    </div>
@endsection