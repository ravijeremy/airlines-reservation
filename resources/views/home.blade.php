@extends('layouts.app')

@section('content')
    <div id="content">
        {{-- Navbar --}}
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span>Menu</span>
                </button>
            </div>
        </nav>
        {{-- End of Navbar --}}

        {{-- City and Set Origin -> Destination --}}
        <div class="row no-gutters">
            <div class="container px-4 py-4">
                <label>Choose City: </label>
                <select name="city" id="city" class="form-control">
                    <option value="">Select City</option>
                    @foreach ($cities as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                    <small id="selectCityHelp" class="form-text text-muted">Choose a city where the bookings take place</small>
                </select>
            </div>
        </div>
        {{-- End of City and Set Origin -> Destination --}}

        {{-- Flights --}}
        <div class="row no-gutters pt-3"> 
            <div class="container px-4 py-4">
                @if (count($flights) > 0)
                    @foreach ($flights as $flight)
                    <div class="row pb-4">
                        <div class="col-3">
                            <img src="{{asset('assets/img/flight-example.jpg')}}" class="rounded" alt="Flight Example" width="200px" height="200px"> 
                        </div>
                        <div class="col">
                            <table>
                                <tr>
                                    <td style="min-width: 122px">Flight No</td>
                                    <td padding="padding-left: 10px; padding-right: 10px">:</td>
                                    <td>{{ $flight->fno }}</td>
                                </tr>
                                <tr>
                                    <tr>
                                        <td style="min-width: 122px">Dept Time</td>
                                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                                        <td>{{ $flight->depttime }}</td>
                                    </tr>
                                    <tr>
                                        <td style="min-width: 122px">Arr Time</td>
                                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                                        <td>{{ $flight->arrtime }}</td>
                                    </tr>
                                    <tr>
                                        <td style="min-width: 122px">Flight Length</td>
                                        <td padding="padding-left: 10px; padding-right: 10px">:</td>
                                        <td>{{ $flight->flen }}</td>
                                    </tr> 
                                </tr>
                                <tr>
                                    <td style="min-width: 122px">Business Seats Available</td>
                                    <td padding="padding-left: 10px; padding-right: 10px">:</td>
                                    <td>{{ $flight->bbusseat }} / {{ $flight->tbusseat }} Total</td>
                                </tr>
                                <tr>
                                    <td style="min-width: 122px">Economics Seats Available</td>
                                    <td padding="padding-left: 10px; padding-right: 10px">:</td>
                                    <td>{{ $flight->becoseat }} / {{ $flight->tecoseat }} Total</td>
                                    </tr>
                            </table>  
                            <a class="btn btn-primary" href="#" role="button">Link</a>
                            <button type="submit" class="btn btn-primary">Book Now</button>
                        </div>
                    </div>  
                    @endforeach
                @endif
            </div>
        </div>
        {{-- End of Flights --}}
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('#city').on('change', function () {
                axios.post('{{ route('city_submit') }}', {id: $(this).val()})
                    .then(function (response) {
                        $('#city').empty();

                        $.each(response.data, function (id, name){
                            $('#city').append(new Option(name, id))
                        })
                    });
            });
        });
    </script>
@endsection