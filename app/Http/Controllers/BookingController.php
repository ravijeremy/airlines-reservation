<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Validator, Redirect, Response;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), 
            [
                'fname' => 'required',
                'lname' => 'required',
                'emailCustomer' => 'required',
                'street' => 'required',
                'city' => 'required',
                'province' => 'required',
                'country' => 'required',
                'postcode' => 'required',
                'pcrtycode' => 'required|max:2',
                'pareacode' => 'required|max:3',
                'pnumber' => 'required|max:7',
                'fcrtycode' => 'required|max:2',
                'fareacode' => 'required|max:3',
                'fnumber' => 'required|max:7'
            ],

            [
                'fname.required' => 'Please fill First Name field.',
                'lname.required' => 'Please fill Last Name field.',
                'emailCustomer.required' => 'Please fill Email field.',
                'street.required' => 'Please fill Street field.',
                'city.required' => 'Please fill City field.',
                'province.required' => 'Please fill Province field.',
                'country.required' => 'Please fill Country field.',
                'postcode.required' => 'Please fill Postal Code field.',
                'pcrtycode.required' => 'Please fill Phone Country Code field.',
                'pareacode.required' => 'Please fill Phone Area Number field.',
                'pnumber.required' => 'Please fill Phone Number field.',
                'fcrtycode.required' => 'Please fill Fax Country Code field.',
                'fareacode.required' => 'Please fill Fax Area Number field.',
                'fnumber.required' => 'Please fill Fax Number field.'
            ]
        );

        $validator->validate();

        dd('Form Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
