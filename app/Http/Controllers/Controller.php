<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function query(Request $request)
    {
        $data['query1'] = DB::table('customer')
                            ->select('customer.*')->where('country', 'CANADA')->orderBy('custid')
                            ->get();

        $data['query2'] = DB::table('booking')
                            ->join('customer', 'booking.custid', '=', 'customer.custid')                    
                            ->select('booking.*', 'customer.custid', 'customer.fname')->orderBy('bkgno')
                            ->get();

        $data['query3'] = DB::table('currency')
                            ->select('currency.*')->where('exchrate', '>', 1)->orderBy('fcurr')
                            ->get();

        $data['query4'] = DB::table('flightavailability')
                            ->join('airport', 'flightavailability.orig', '=', 'airport.airportcd')
                            ->where('orig', '=', 'YYZ')
                            ->orWhere('orig', '=', 'JFK')
                            ->select('flightavailability.*', 'airport.airportcd')
                            ->get();

        $data['query5'] = DB::table('customer')
                            ->whereNotIn('custid', function($q){
                                $q->select('custid')->from('booking');
                            })
                            ->get();

        $data['query6'] = DB::table('customer')
                            ->select('customer.custid', 'customer.fname', 'customer.lname', 'phone.pnumber', 'email.email')
                            ->join('phone', 'customer.custid', '=', 'phone.custid')
                            ->join('email', 'customer.custid', '=', 'email.custid')
                            ->orderBy('customer.custid')
                            ->get();

        $data['query7'] = DB::table('booking')
                            ->select('booking.bkgno', 'booking.custid', 'booking.fno', 'booking.orig', 'booking.dest', 'booking.bkgcity', 'status.status', 'classes.classtype', 'city.citynm')
                            ->join('status', 'booking.statusid', '=', 'status.statusid')
                            ->join('classes', 'booking.classid', '=', 'classes.classid')
                            ->join('city', 'booking.bkgcity', '=', 'city.cityid')
                            ->where('booking.statusid', '=', '2')
                            ->orderBy('booking.bkgno', 'asc')
                            ->orderBy('booking.custid', 'asc')
                            ->orderBy('booking.fno')
                            ->get();

        $data['query8'] = DB::table('booking')
                            ->select('booking.bkgcity', 'booking.totprice', 'booking.paidamt', 'booking.bal')
                            ->whereNotIn('booking.statusid', ['3'])
                            ->orderBy('booking.bkgcity')
                            ->get();

        $data['query9'] = DB::table('booking')
                            ->select('booking.bkgno', 'booking.orig', 'booking.dest', 'booking.fprice', 'booking.totprice as prevprice',
                            DB::raw('booking.totprice +
                                        booking.fprice*(select airporttax+0.01 from airport where airport.airportcd=booking.orig) +
                                        booking.fprice*(select airporttax+0.005 from airport where airport.airportcd=booking.dest)as newprice'))
                            ->get();

        $data['query10'] = DB::table('customer')
                            ->select('customer.custid', 'customer.fname', 'customer.lname',
                            DB::raw('count(distinct booking.bkgno) as totbookings'),
                            DB::raw('count(distinct email.email) as totemails'),
                            DB::raw('count(distinct fax.fnumber) as totfaxs'),
                            DB::raw('count(distinct phone.pnumber) as totphones'))
                            ->leftjoin('booking', 'booking.custid', '=', 'customer.custid')
                            ->leftjoin('email', 'email.custid', '=', 'customer.custid')
                            ->leftjoin('fax', 'fax.custid', '=', 'customer.custid')
                            ->leftjoin('phone', 'phone.custid', '=', 'customer.custid')
                            ->groupBy('customer.custid', 'customer.fname', 'customer.lname')
                            ->orderBy('customer.custid')
                            ->get();

        $data['queryCustom'] = DB::raw($request->queryCustom);

        return view('query', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
