<?php

namespace App\Http\Controllers;

use App\City;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class AirlineController extends Controller
{
    public function index()
    {
        $cities = City::pluck('citynm', 'cityid');

        $flights = DB::table('flightavailability')
                    ->select('flightavailability.*')
                    ->get();

        return view('home')->with('cities', $cities)->with('flights', $flights);
    }

    public function store(Request $request)
    {
        $cities = City::where('cityid', $request->get('cityid'))->pluck('citynm', 'cityid');

        return response()->json('$cities');
    }
}
