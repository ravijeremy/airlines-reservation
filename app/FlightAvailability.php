<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlightAvailability extends Model
{
    protected $table = 'flightavailability';

    public $primaryKey = 'fno';
}
