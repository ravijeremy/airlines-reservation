<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AirlineController@index');
Route::post('/', 'AirlineController@store')->name('city_submit');
Route::get('/input-data', function (){
    return view('customer');
});
Route::post('/input-data', 'BookingController@store');


Route::get('/query', 'Controller@query')->name('query');
Route::post('/query', 'Controller@query')->name('query_submit');